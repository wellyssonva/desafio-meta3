# Criar tela de cadastro e consulta para usuários e funcionários#

A solution contem uma estrutura que deve ser seguida na criação de novas funcionalidades.

### **Fucionalidades:** ###
* Implementar telas para manipulação de usuários.
	1. Cadastro de Usuário.
		* Para a mesma tela deve ser possível, realizar as operações CRUD.
	2. Listagem de Usuários.
		* Nesta dela deve ser possível realziar filtros de usuários pelo nome.
		* Selecionar usuário para edição. Redirecionar para tela de cadastro.

* Implementar uma tela para manipulação de Funcionários.
	3. Cadastro de Funcionário.
		* Para a mesma tela deve ser possível, realizar as operações CRUD.
	4. Listagem de Funcionários.
		* Nesta dela deve ser possível realizar filtros funcionários pelo __Nome__ e ou por __Função__.
		* Selecionar funcionário para edição. Redirecionar para tela de cadastro.

### **Deve-se implementar** ###

* Utilização de JQuery.
* Testes unitários.
* Implementação de boas práticas.

### **Diferencial** ###

* Desnenvolvimento de telas organizadas e intuitivas.

### **Submeter para avaliação** ###

O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:

1. Candidato fará um fork desse repositório (não deve ser clonado)
2. Fará seu projeto nesse fork.
3. Commitará e subirá as alterações para o __SEU__ fork.
4. Pela interface do Bitbucket, irá enviar um Pull Request.

Deixar o fork público para facilitar a inspeção do código.