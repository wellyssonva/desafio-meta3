﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Scripts
{
    public static class Usuario
    {
        public const string ObterUsuarioPorLogin = @"SELECT	u.Id,
                                                            u.IdPessoaFisica,
                                                            pf.Nome,
                                                            u.Login,
                                                            u.Senha,
                                                            u.DataInclusao,
                                                            u.Ativo 
                                                    FROM Usuario u
                                                    INNER JOIN PessoaFisica pf ON pf.id = u.IdPessoaFisica
                                                    WHERE Login like @Login";

        public const string ValidaLogin = @"SELECT	count(1)
                                            FROM Usuario WHERE Login = @Login
                                            and Senha = @Senha";


        public const string ObterListaUsuarios = @"SELECT	u.Id,
                                                            u.IdPessoaFisica,
                                                            u.Login,
                                                            u.Senha,
                                                            u.DataInclusao,
                                                            u.Ativo
                                                            
                                                    FROM Usuario u
                                                    ";

        public const string ObterUsuarioPorId = @"SELECT	u.Id,
                                                            u.IdPessoaFisica,
                                                            u.Login,
                                                            u.Senha,
                                                            u.DataInclusao,
                                                            u.Ativo
                                                                    FROM Usuario u        
                                                                 WHERE u.Id = @IdUsuario";

        public const string AtualizarUsuario = @"UPDATE Usuario set 
                                                        Login = @Login,
                                                        Senha = @Senha,
                                                        DataInclusao = @DataInclusao,
                                                        Ativo = @Ativo,
                                                        IdPessoaFisica = @IdPessoaFisica
                                                        FROM Usuario
                                                        WHERE Id = @IdUsuario";

       

        public const string DeletarUsuario = @"DELETE FROM Usuario WHERE Id = @IdUsuario ";

        public const string InserirUsuario = @"INSERT INTO Usuario (Login, Senha, DataInclusao, Ativo)
                                              VALUES (@Login, @Senha, @DataInclusao, @Ativo)";

       }
}
